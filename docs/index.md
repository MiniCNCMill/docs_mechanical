# Welcome to MiniCNCMill documentation

MiniCNCMill is an opensource project, which aims to offer beginners and interested people a tutorial or introduction to the topic of CNC milling.

**Important**: The distinction between conventional CNC milling and the more common 2D milling aka 'routing' in the hobby is crucial here. This project is about conventional CNC milling.

Drawings, circuit diagrams as well as schematics will be available on the following pages. Furthermore the necessary settings in LinuxCNC for the machine as well as for a suitable simulator will be prepared.

## Actual Image of existing machine ##
![machine](images/photo_2021-12-20_21-40-07.jpg)